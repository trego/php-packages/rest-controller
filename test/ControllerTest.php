<?php

namespace Trego\RestController\Test;

use PHPUnit\Framework\TestCase;
use Trego\RestController\RestController;

class ControllerTest extends TestCase
{
    public function testConstructClass()
    {
        $rest = $this->getMockForAbstractClass(RestController::class);

        $this->assertInstanceOf(RestController::class, $rest);
    }
}
