<?php

namespace Trego\RestController;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\ResourceInterface;

abstract class RestController extends Controller
{
    /**
     * Stores Fractal Manager instance.
     *
     * @var Manager
     */
    protected $manager;

    /**
     * Stores TransformerAbstract class.
     *
     * @var string
     */
    protected $transformer;

    public function __construct()
    {
        $this->manager = new Manager();
    }

    /**
     * Parse relationships from URI.
     *
     * @param Request $request
     */
    protected function parseRelationships(Request $request)
    {
        if (!empty($request->get('include'))) {
            $this->manager->parseIncludes($request->get('include'));
        }
    }

    /**
     * Generate model into transformer.
     *
     * @param $model
     * @param $transformer
     *
     * @return Item
     */
    protected function generateItem($model, $transformer = null)
    {
        if (!is_null($transformer)) {
            return new Item($model, new $transformer());
        }

        return new Item($model, new $this->transformer());
    }

    /**
     * Generate collections into transformer.
     *
     * @param $model
     * @param $transformer
     *
     * @return Collection
     */
    protected function generateCollection($model, $transformer = null)
    {
        if (!is_null($transformer)) {
            return new Collection($model, new $transformer());
        }

        return new Collection($model, new $this->transformer());
    }

    /**
     * Send response.
     *
     * @param ResourceInterface $data
     * @param int               $status
     *
     * @return Illuminate\Http\Response
     */
    protected function response(ResourceInterface $data, $status = 200)
    {
        return response()->json(
            $this->manager->createData($data)->toArray(),
            $status
        );
    }

    /**
     * Send 404 error.
     *
     * @param string $error
     *
     * @return Illuminate\Http\Response
     */
    protected function sendNotFoundResponse($error)
    {
        return response()->json($error, 404);
    }

    /**
     * Send 500 error.
     *
     * @param string $error
     *
     * @return Illuminate\Http\Response
     */
    protected function sendIseResponse($error)
    {
        return response()->json($error, 500);
    }

    /**
     * Send 401 error.
     *
     * @param string $error
     *
     * @return Illuminate\Http\Response
     */
    protected function sendUnprocessableEntityResponse($error)
    {
        return response()->json($error, 422);
    }

    /**
     * Send 400 error.
     *
     * @param string $error
     *
     * @return Illuminate\Http\Response
     */
    protected function sendBadRequestResponse($error)
    {
        return response()->json($error, 400);
    }

    /**
     * Send 401 error.
     *
     * @param string $error
     *
     * @return Illuminate\Http\Response
     */
    protected function sendUnauthorizedResponse($error)
    {
        return response()->json($error, 401);
    }

    /**
     * Send 403 error.
     *
     * @param string $error
     *
     * @return Illuminate\Http\Response
     */
    protected function sendForbiddenResponse($error)
    {
        return response()->json($error, 403);
    }
}
